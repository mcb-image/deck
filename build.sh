#!/bin/bash
set -e

banner() {
    echo "================================================================"
    echo "$1"
    echo "================================================================"
}

PROJECT="deck"

banner "Building docker image "$PROJECT" ..."
docker build -t $PROJECT .
banner "Copying the binary..."
docker run $PROJECT deck version
