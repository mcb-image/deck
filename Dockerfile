FROM golang:1.11.4-stretch
RUN apt-get update && apt-get install -y unzip
RUN mkdir -p /go/src
WORKDIR /go/src
ENV VERSION 0.4.0
RUN wget https://github.com/hbagdi/deck/archive/v$VERSION.zip
RUN unzip v$VERSION.zip -d .
RUN mv deck-$VERSION deck
RUN pwd
WORKDIR /go/src/deck
RUN go get .
RUN CGO_ENABLED=0 GOOS=linux go install -a -ldflags '-s -w -extldflags "-static"' .


FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN mkdir -p /root/bin
RUN mkdir /data
WORKDIR /data
COPY --from=0 /go/bin/deck /root/bin/
CMD ["--help"]
ENTRYPOINT ["/root/bin/deck"]

